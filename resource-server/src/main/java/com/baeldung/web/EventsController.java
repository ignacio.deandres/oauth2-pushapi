package com.baeldung.web;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventsController {

    @PostMapping("/events")
    @ResponseBody
    public String postEvents() {
        return "Events has been received";
    }
}